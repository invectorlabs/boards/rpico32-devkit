# RPICO32 Evaluation Kit (With 5 sample modules)

The RPICO32 Evaluation Kit is an evaluation system for our own RP2040 based WiFi module RPICO32 !

The RPICO32 module is a powerful embedded systems module based on the RP2040, from the Raspberry Pi foundation, connected to a ESP8285 WiFi chip from Espressif Systems. The module is also equipped with 8Mbyte of FLASH memory for the RP2040 as well as all the required crystals and a properly tuned antenna.

More information and technical details on this module are available here.

Also included with the kit is 5 samples of the RPICO32 module, allowing you to kickstart your next design.

# Evaluation board details

This evaluation board contains all the necessary electronics (which is not much) needed to get the module running so that you can start downloading and debug code.

In addition to this, it also includes circuitry for charging and managing a LiPo battery cell allowing you to test the system in battery-operated situations. We also included a current measurement resistor to simplify measuring the actual current used by the module in different scenarios.

## USB Type C

To communicate with the device there is a USB-C connector on the board. This is connected directly to the USB pins of the module which allows you to connect to the module to download code or do any other type of communication as required by your system.

## GPIO pin headers

Two rows of pin headers are available that are directly connected to the GPIO pins of the module. This allows you to access all the GPIO pins and other hardware block to start testing your stuff out.

Aref can can be left connected to 3.3V or supplied separately if required.
Buttons

Two buttons are available on the board to be able to force a reset or to move the module into UF2 boot loader mode.

## JTAG

A 10-pin narrow pitch JTAG header is available on the board allowing you to hook a proper JTAG debugger to the device and do some heavy lifting debugging on it.
